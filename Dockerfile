FROM richarvey/nginx-php-fpm:latest

ENV SERVER_PATH=/var/www
ENV SOURCE_PATH=${SERVER_PATH}/src
ENV PUBLIC_PATH=${SERVER_PATH}/html

WORKDIR ${SERVER_PATH}
COPY composer.* ./
RUN composer install && composer update

COPY config/nginx.conf /etc/nginx/sites-available/default.conf
COPY src/ ${SOURCE_PATH}
COPY public/ ${PUBLIC_PATH} 

