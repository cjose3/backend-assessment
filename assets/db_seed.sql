CREATE TABLE stats(
    name VARCHAR(24) PRIMARY KEY,
    value FLOAT NOT NULL DEFAULT 0.0
);

INSERT INTO stats VALUES ('stats db 1', 1.2);
INSERT INTO stats VALUES ('stats db 2', 9.5);
INSERT INTO stats VALUES ('stats db 3', 112);
