<?php
namespace App\Services;

/*
 * The services are responsible of:
 *  - Save data in databases
 *  - Connect to third party services
 *  - Calculate data
 *  - Retrieve data from databases
 *  - etc.
 */

use App\Services\MockApiService;
use App\Services\DBService;

/**
 * 
 */
class StatsService {

    public static function find($query) {
        $mockStats = MockApiService::getStats($query);
        $dbStats = DBService::getStats($query);
        return array_merge($mockStats, $dbStats);
    }

    public static function create($stats) {
        $record = DBService::updateOrCreateStats($stats);
        return $record;
    }

}