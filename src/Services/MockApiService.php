<?php
namespace App\Services;

use Requests;

define('URL', $_ENV['MOCK_API_URL']);


class MockApiService {

    public static function getStats() {
        $headers = ['Accept' => 'application/json'];
        $request = Requests::get(URL.'/stats', $headers);
        return json_decode($request->body);
    }

}