<?php
namespace App\Services;

define('DB_URI', $_ENV['DB_URI']);
define('DB_USERNAME', $_ENV['DB_USERNAME']);
define('DB_PASSWORD', $_ENV['DB_PASSWORD']);

class DBService {
    
    public static function getStats($query = []) {
        $sql = 'SELECT * FROM stats';        
        $conn = new \PDO(DB_URI, DB_USERNAME, DB_PASSWORD);
        $sth = $conn->query($sql, \PDO::FETCH_ASSOC);
        $stats = $sth->fetchAll();
        $conn = null;
        $sth = null;
        return $stats;
    }

    public static function updateOrCreateStats($stats = []) {
        $name = $stats['name'];
        $value = $stats['value'];

        $conn = new \PDO(DB_URI, DB_USERNAME, DB_PASSWORD);

        $sql = 'INSERT INTO stats (name, value) values (?, ?)';
        $sth = $conn->prepare($sql);
        $sth->execute([$name, $value]);
        
        $sql = "SELECT * FROM stats WHERE name = '$name'";
        $sth = $conn->query($sql, \PDO::FETCH_ASSOC);
        $stats = $sth->fetchAll();
        
        $conn= null;
        $sth = null;
        return $stats;
    }
}