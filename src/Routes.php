<?php
namespace App;

/*
 * All routes should be defined in this file.
 * Each route will be a array with the format:
 * ['METHOD', 'resource', controller's method]
 * 
 * For more information about the router see: https://github.com/nikic/FastRoute
 */

return [
    ['GET', '/stats', 'StatsController::find'],
    ['POST', '/stats', 'StatsController::create']
];