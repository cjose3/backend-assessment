<?php
namespace App\Controllers;

/*
 * The controllers must only use for:
 *  - Get params from $request
 *  - Get body from $request
 *  - Call Services to retrieve or save data
 *  - Build response's content
 *  - Send response using $response
 */

use App\Services\StatsService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for the path: /stats
 */
class StatsController {

    /*
     * GET /stats
     */
    public static function find(Request $request, Response $response) {
        $query = $request->query->all();
        $data = StatsService::find($query);
        $response->setData($data)
            ->setStatusCode(Response::HTTP_OK)
            ->send();
    }

    /*
     * POST /stats
     */
    public static function create(Request $request, Response $response) {
        $body = $request->request->all();
        $data = StatsService::create($body);
        $response->setData($data)
            ->setStatusCode(Response::HTTP_CREATED)
            ->send();
    }
}