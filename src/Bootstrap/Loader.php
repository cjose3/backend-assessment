<?php
namespace App\Bootstrap;

/*
 * Load all files in the folders:
 *  - Controllers
 *  - Services
 */

$controllers = glob(__DIR__ . '/../Controllers/*Controller.php');
$services = glob(__DIR__ . '/../Services/*Service.php');

foreach ($controllers as $controller) {
    require($controller);   
}

foreach ($services as $service) {
    require($service);   
}