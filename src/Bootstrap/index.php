<?php
namespace App\Bootstrap;

require __DIR__ . '/../../vendor/autoload.php';

require 'ErrorHandler.php';
require 'Loader.php';
require 'RouteDispatcher.php';