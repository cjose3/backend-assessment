<?php
namespace App\Bootstrap;

/*
 * Represents an HTTP request.
 * 
 * For more information about the object see: http://api.symfony.com/4.0/Symfony/Component/HttpFoundation/Request.html
 */
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;

$request = Request::createFromGlobals();

/*
 *
 * If the request is a JSON, it will be converted to array associative
 * The array will be in `$request->request->all()`
 *
 * since: https://silex.symfony.com/doc/2.0/cookbook/json_request_body.html
 */
if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
    $data = json_decode($request->getContent(), true);
    $request->request->replace(is_array($data) ? $data : array());
}

return $request;