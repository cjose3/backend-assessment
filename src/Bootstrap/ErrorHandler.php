<?php
namespace App\Bootstrap;

/*
 * This script register a error handler.
 * 
 * For more information about it see: https://github.com/filp/whoops
 */

use Whoops\Run;
use Whoops\Handler\PrettyPageHandler;

error_reporting(E_ALL);

$environment = $_ENV['PHP_ENVIRONMENT'];

$whoops = new Run;
if ($environment !== 'production') {
    $whoops->pushHandler(new PrettyPageHandler);
}
$whoops->register();
