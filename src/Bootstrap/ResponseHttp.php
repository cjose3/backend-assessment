<?php
namespace App\Bootstrap;

/*
 * Represents an HTTP response.
 * 
 * For more information about the object see: http://api.symfony.com/4.0/Symfony/Component/HttpFoundation/JsonResponse.html
 */

use Symfony\Component\HttpFoundation\JsonResponse;


class AppResponse extends JsonResponse {
    
    public function sendError($message) {
        return $this->setData($message, true)->send();
    }
    
    public function setData($data = array(), $error = false) {
        $rawData = [
            'error' => $error,
            'message' => '',
            'data' => ''
        ];
        $rawData[$error ? 'message' : 'data'] = $data;
        return parent::setData($rawData);
    }
}

return new AppResponse();