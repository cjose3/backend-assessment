<?php
namespace App\Bootstrap;

/*
 * This script load and handle all routes from `src/Routes.php` file
 * 
 * For more information about the router see: https://github.com/nikic/FastRoute
 */

use Symfony\Component\HttpFoundation\Response;
use FastRoute\RouteCollector;
use FastRoute\Dispatcher;

$request = require 'RequestHttp.php';
$response = require 'ResponseHttp.php';

$method = $request->getMethod();
$uri = $request->getPathInfo();
$dispatcher = createDispatcher();
$dispatchedRoute = $dispatcher->dispatch($method, $uri);

handlerDispatchedRoute($dispatchedRoute, $request, $response);

/* 
 * -------------------------------------------------------
 * ------------- FUNCTIONS 
 * -------------------------------------------------------
 */

function createDispatcher() {
    return \FastRoute\simpleDispatcher(function(RouteCollector $collector) {
        $routes = require __DIR__.'/../Routes.php';
        foreach ($routes as $route) {
            $collector->addRoute($route[0], $route[1], $route[2]);
        }
    });
}

function handlerDispatchedRoute($dispatchedRoute, $request, $response) {
    switch ($dispatchedRoute[0]) {
        
        case Dispatcher::NOT_FOUND:
            $response
                ->setStatusCode(Response::HTTP_NOT_FOUND)
                ->sendError('404 - Page not found');
            break;

        case Dispatcher::METHOD_NOT_ALLOWED:
            $response
                ->setStatusCode(Response::HTTP_NOT_ALLOWED)
                ->sendError('405 - Method not allowed');
            break;
        
        case Dispatcher::FOUND:
            try {
                call_user_func(
                    '\\App\\Controllers\\'.$dispatchedRoute[1], 
                    $request, 
                    $response
                );
            } catch (\Exception $ex) {
                $response
                    ->setStatusCode(Response::HTTP_BAD_REQUEST)
                    ->sendError($ex->getMessage());
            }
            break;
    }
}
