# Backend Assessment

## Dependencies

* Docker
* Docker Compose
* Make

## Build app

`make build`

## Start app

`make up`

The app run in `http://localhost:8080`

## View logs

`make logs`

## Stop app

`make stop`