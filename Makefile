AUTHOR=cjose3
PROJECT_NAME=backend-assessment 
PROJECT_VERSION=1.0.0 

IMAGE=$(AUTHOR)/$(PROJECT_NAME):$(PROJECT_VERSION)

build: ;@docker-compose build

up: ;@make build && docker-compose up -d

logs: ;@docker-compose logs -f app

stop: ;@docker-compose stop

.PHONY: test